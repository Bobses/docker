Create a new Wordpress website using Docker!

1. Change passwords and username in the **.env** file.
2. Go into this folder: docker/docker-compose/wordpress
3. Run the following command:
`docker-compose up -d`
4. Wait...
5. Check the newly created docker containers using the `docker ps` command.
6. In your favourite web browser, got to the following addresses:

* for Wordpress: http://localhost:8888/
* for phpMyAdmin: http://localhost:3001/

7. Enjoy!

If you look in your Wordpress folder, an extra folder has been created:

```
WordPress
    |   wordpress-files
    |   +-- docker-compose.yml
    |   +-- .env
```    

## Useful commands

`docker-compose up -d`          Start your services defined in docker-compose.yml

`docker-compose down`           Stop services

`docker-compose restart`        Restart services

`docker network ls`             Show docker networks

`docker volume ls`              Show docker volumes

`docker inspect <containerId>`  Inspect container with containerId *

*you can see the containerId when you execute the command `docker ps`.