Images that can be used for the **reveal.js** presentations:

- bobses/tux:reveal.js-alpine3.12 (based on Alpine 3.12.0) - recommended (the smalest size)
- bobses/tux:reveal.js-node12 (based on Debian 9 *stretch*)
- bobses/tux:presentations-f32 (based on Fedora 32)

You can change the image according to your preference when running the **reveal.js** presentation.