This is my presentation about **JSONPath in Kubernetes**. I wrote about it for the first time on my blog: https://www.despre-linux.eu/jsonpath-kubernetes/

To run this dockerized presentation you have to have docker installed on your computer: https://docs.docker.com/get-docker/.

## How to run

### 1. With DOCKER

1. Clone the repo: `git clone https://gitlab.com/Bobses/docker.git`
2. Go into the **docker/reveal.js-presentations/jsonpath-in-k8s** directory.
3. Run the following command:

```sh
docker run -d -v $(pwd)/slides/:/reveal.js/slides/ -p 8000:8000 --name slides bobses/tux:reveal.js-alpine3.12
```

4. Open your web browser and go to http://localhost:8000/.
5. That's all! Dive and browse into the slides! Enjoy!


### 2. In Kubernetes

First of all, you have to edit the **jsonpath-k8s-po.yaml** and change the value from `.spec.volumes[0].hostPath.path` with the your current path tot the **slides** directory.

I assume that you have **microk8s**, **minikube** or even a full kubernetes cluster installed.

#### a. microk8s case

Run the following command:

```
microk8s kubectl apply -f jsonpath-k8s-pod.yaml
```

#### b. minikube case

Run the following command:

```
kubectl apply -f jsonpath-k8s-pod.yaml
```

The command will create a **pod** named *jsonpath* and a **nodePort service** named *jsonpath-service*.

In both cases, you can access the presentation in your browser typing this URL: http://localhost:30008

## Play!

After watching the presentation, try to play with jsonPath. :) As an example, you can find the hostPath using one of the following command:

* `kubectl get po -o=jsonpath='{.items[*].spec.volumes[0].hostPath.path}'`
* `kubectl get po -o=custom-columns=POD:.metadata.name,Mount:.spec.volumes[0].hostPath.path`


The presentation is based on the **reveal.js** project: https://revealjs.com/.
