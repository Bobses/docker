![JsonPath in Kubernetes](../slides/images/json-path-kubernetes.png)

---

This was first presented on my blog [despre-linux.eu](https://www.despre-linux.eu/jsonpath-kubernetes/)

---

* Why JSONPath?
* View kubectl output in JSON format
* How to use JSONPath with kubectl
* Loops – Range
* Custom Columns
* Sort
* JSONPath Examples
* Resources

---

## Why JSONPath?

-

<div style="text-align: left">Large data sets:</div>

* hundreds of nodes
* thousands of pods
* thousands of other k8s resources (deployments, services, etc.) 

-

Using only the **kubectl** command (the k8s cli) for reading and describing resources can become an overwhelming task.

---

![Kubectl command](../slides/images/kubectl.png)

-

A lot of information is hidden (architecture, resource capacity, taints, etc.): 

![Hidden information](../slides/images/kubectl-get-nodes.png)

-

More information can be visualised using the following commands:

```sh
kubectl get nodes -owide
```
![kubectl get nodes -owide](../slides/images/kubectl-get-nodes-owide.png)

or

```sh
kubectl describe node ...
```

---

## View kubectl output in JSON format

-

As I previous said, the **apiserver** send the information about a k8s resource in json format:

```bash
kubectl describe node node_name -ojson
```

![json-kubectl-get-nodes](../slides/images/json-kubectl-get-node.png)

-


Study and familiarize with the output, find the desired path and build the JSON Query.

With the JSONPath Query, we can filter and format the output as we like.

---

## How to use JSONPath with kubectl

-

<div style="text-align: left">Steps:</div>

1. Identify the kubectl command
2. Familiarize with the JSON output
3. Build the JSONPath Query
```sh
.items[*].metadata.name
```
4. Identify the right kubectl command using the above query
```sh
kubectl get no -o=jsonpath='{ .items[*].metadata.name }'
```
-
### Identify the path

-

![Identify the path](../slides/images/jsonpath-metadata-name.png)

---
## JSONPath examples

-

```
kubectl get nodes -o=jsonpath='{ .items[*].metadata.name }'
```
>kube-master worker0 worker1


```
kubectl get no -o=jsonpath='{ .items[*].status.nodeInfo.architecture }'
```
>amd64 amd64 amd64

```
kubectl get no -o=jsonpath='{ .items[*].status.capacity.cpu }'
```
>2 2 2 

-

```
kubectl get po -o=jsonpath='{ .items[*].spec.containers[0].image }'
```
>nginx

```
kubectl get nodes -o=jsonpath='{ .items[*].metadata.name }\
 { .items[*].status.capacity.cpu }'
```
>kube-master worker0 worker12 2 2

The last outcome looks ugly, huh? :)

---

## Loops - Range

-

For each item (node) print its name, insert a TAB as a separator, print the property of each item (the number of CPUs) followed by the newline character and make the iteration again.
```
kubectl get nodes -o=jsonpath='{range .items[*]} {.metadata.name} {"\t"}\
 {.status.capacity.cpu} {"\n"} {end}'
```

![kubectl-combinat-json-range](../slides/images/json-kubectl-combinat-range.png)

-

![jsonpath loops](../slides/images/jsonpath-loops.png)

---

## Custom columns

-

```sh
kubectl get nodes -o=custom-columns=<COLUMN NAME>:<JSONPath Query>
```
```sh
kubectl get no -o=custom-columns=NODE:.metadata.name,\
   CPU:.status.capacity.cpu
```

![example jsonpath-custom-columns](../slides/images/jsonpath-custom-columns.png)

Note that the keyword <span style="color:red">**_.items[*]_**</span> is excluded because the custom-columns option assumes that, by default, the query is made for each element from the list.

---

## JSONPath for Sort

-

JSONPath can be used for sorting objects, specifying the <span style="color:red">**_--sort-by_**</span> option:

```
kubectl get nodes --sort-by=.metadata.name
```

```
kubectl get persistentvolumes --sort-by=.spec.capacity.storage
```

-

Let's see an example:

![jsonpath sort-by example](../slides/images/jsonpath-sort-by.png)

---

## More JSONPath examples

-

```
kubectl get nodes -o=custom-columns=NODE:.metadata.name,\
    OS:.status.nodeInfo.osImage
```
```
kubectl get pv -o=custom-columns=NAME:.metadata.name,\
    CAPACITY:.spec.capacity.storage --sort-by=.spec.capacity.storage
```

-

Print the node name and its internal IP:
```
kubectl get nodes -o=custom-columns=NODE:.metadata.name,\
    InternalIP:.status.addresses[0].address
```

![jsonpath internal ip](../slides/images/jsonpath-internal-ip.png)

-

Select the READY nodes and print the effect of their taint ---> For each item in the node list print the name, print the taint effect and, check if the node is READY:

```
kubectl get nodes -o=jsonpath='{range .items[*]} {.metadata.name} {"\t"}\
     {.spec.taints[*].effect} {"\t"}\
     {.status.conditions[?(@.type == "Ready")].type}\
      {"\n"} {end}'
```      

>Note: - <span style="color:red">**?(@.type == "Ready"**</span> ---> check if **(?)** each item in the list **(@)** has the **type = Ready**

```
kubectl get nodes -o=custom-columns=NODE:.metadata.name,\
    TAINT:.spec.taints[*].effect,\
    STATUS:.status.conditions[3].type
```
-

![jsonpath node ready](../slides/images/jsonpath-node-ready.png)

---
## Resources

* JSONPath support: https://kubernetes.io/docs/reference/kubectl/jsonpath/
* kubectl Cheat Sheet: https://kubernetes.io/docs/reference/kubectl/cheatsheet/#viewing-finding-resources
* Introduction to JSONPath: https://www.baeldung.com/guide-to-jayway-jsonpath
* JSONPath Online Evaluator: http://jsonpath.com/


---

# Enjoy!

Visit [@despre-linux.eu](https://despre-linux.eu)!  